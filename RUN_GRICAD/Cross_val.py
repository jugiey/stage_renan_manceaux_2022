import numpy as np
import pandas as pd
import utils
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import plot_confusion_matrix
import datetime
from tqdm import tqdm

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
import xgboost as xgb
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn import svm
from sklearn.naive_bayes import GaussianNB

import argparse

print('Start:',datetime.datetime.now())

################################
# Settings arg parser
################################

parser = argparse.ArgumentParser()
parser.add_argument('-mod', '--model') # {RF, GB, XGB, KNN, kmeans, SVM, GNB}

parser.add_argument('-cw', '--class_weight',default=None) # {None, "balanced", or float}
parser.add_argument('-md', '--max_depth',default=None) # {None, or int > 0}
parser.add_argument('-nn', '--n_neighbors',default=5) # int > 0
parser.add_argument('-m', '--metric',default="minkowski") # {"minkowski", "chebyshev"}
parser.add_argument('-p', '--p',default=2) # int > 0 : 1 -> manhattan, 2 -> euclidian
parser.add_argument('-w', '--weights',default="uniform") # {"uniform","distance"}
parser.add_argument('-k', '--kernel',default='rbf') # {"linear","poly","rbf","sigmoid"}
parser.add_argument('-d', '--degree',default=3) # int > 0
parser.add_argument('-c', '--C',default=1.0) # float > 0
parser.add_argument('-prob', '--probability',default=0) # 0 or 1

parser.add_argument('-s', '--sample',default="all") # {"all","under","over","threshold"}
parser.add_argument('-v', '--var',default="all") # {"all","set1","set2"}

args = parser.parse_args()

################################
# Model selection
################################

mod = args.model

################################
# Setting parameters
################################

# Random Forest
if mod == "RF": 
    if args.class_weight == None:
        class_weight = None
    elif args.class_weight == "balanced":
        class_weight = args.class_weight
    else:
        class_weight = {1: float(args.class_weight)}
    if args.max_depth == None:
        max_depth = None
    else:
        max_depth = int(args.max_depth)

    cl = RandomForestClassifier(max_depth=max_depth, class_weight=class_weight)
    print(mod, " cross validation : ")
    print('Class weight : ',class_weight)
    print("max depth : ",max_depth)

    if args.sample != "all":
        file_name = mod + "-" + args.sample + "-" + "cw" + str(args.class_weight) + "-" + "md" + str(args.max_depth)
    else:
        file_name = mod + "-cw" + str(args.class_weight) + "-" + "md" + str(args.max_depth)

    
# Gradient Boost
if mod == "GB":
    if args.max_depth == None:
        max_depth = None
    else:
        max_depth = int(args.max_depth)

    cl = GradientBoostingClassifier(max_depth=max_depth)
    print(mod, " cross validation : ")
    print("max depth : ",max_depth)

    if args.sample != "all":
        file_name = mod + "-" + args.sample + "-" + "md" + str(args.max_depth)
    else:
        file_name = mod + "-" + "md" + str(args.max_depth)

# XGBoost
if mod == "XGB":
    cl = xgb.XGBClassifier()
    print(mod, " cross validation : ")

    if args.sample != "all":
        file_name = mod + "-" + args.sample
    else:
        file_name = mod

# KNN
if mod == "KNN":
    n_neighbors = int(args.n_neighbors)
    metric = args.metric
    p = int(args.p)
    weights = args.weights

    cl = KNeighborsClassifier(n_neighbors = n_neighbors, metric = metric, p = p, weights=weights)
    print(mod, " cross validation : ")
    print("n_neighbors : ",n_neighbors)
    print("metric : ",metric)
    print("p : ",p)
    print('weights : ',weights)

    if args.sample != "all":
        file_name = mod + "-" + args.sample + "-n" + str(args.n_neighbors )+ "-" + args.metric + str(args.p) + "-" + args.weights 
    else:
        file_name = mod + "-n" + str(args.n_neighbors) + "-" + args.metric + str(args.p) + "-" + args.weights

# kmeans
if mod == "kmeans":
    cl = KMeans(2)
    print(mod, " cross validation : ")

    if args.sample != "all":
        file_name = mod + "-" + args.sample
    else:
        file_name = mod

# SVM
if mod == "SVM":
    k = args.kernel
    degree = int(args.degree)
    C = float(args.C)
    if args.class_weight == None:
        class_weight = None
    elif args.class_weight == "balanced":
        class_weight = args.class_weight
    else:
        class_weight = {1: float(args.class_weight)}
    probability = bool(int(args.probability))

    cl = svm.SVC(kernel=k,class_weight=class_weight,C=C,degree=degree,probability=probability)
    print(mod, " cross validation : ")
    print('kernel : ',k)
    if k == "poly" :
        print("degree : ",degree)
        k = "poly"+str(args.degree)
    print('class weight : ',class_weight)
    print("C : ",C)
    print("probability : ",probability)


    if args.sample != "all":
        file_name = mod + "-" + args.sample + "-" + k + "-" + "cw" + str(args.class_weight) + "-C" + str(C)
    else:
        file_name = mod + "-" + k + "-" + "cw" + str(args.class_weight) + "-C" + str(C)
    
if mod == "GNB":
    cl = GaussianNB()
    print(mod, " cross validation : ")
    if args.sample != "all":
        file_name = mod + "-" + args.sample
    else:
        file_name = mod

################################
# Setting dataset
################################

print("importing data")
# import data
CI02 = utils.load_data("data/CI02.npy")
CI04 = utils.load_data("data/CI04.npy")
CI06 = utils.load_data("data/CI06.npy")
CI09 = utils.load_data("data/CI09.npy")
CI21 = utils.load_data("data/CI21.npy")

# variable selection
if args.var == 'set1':
    CI02 = CI02[['Y','dist2GB','dist2TJ','schmid',
                'diff_schmid','misangle','eqStrain',
                'eqStress','act_pr','act_py','work']] 
    CI04 = CI04[['Y','dist2GB','dist2TJ','schmid',
                'diff_schmid','misangle','eqStrain',
                'eqStress','act_pr','act_py','work']] 
    CI06 = CI06[['Y','dist2GB','dist2TJ','schmid',
                'diff_schmid','misangle','eqStrain',
                'eqStress','act_pr','act_py','work']] 
    CI09 = CI09[['Y','dist2GB','dist2TJ','schmid',
                'diff_schmid','misangle','eqStrain',
                'eqStress','act_pr','act_py','work']] 
    CI21 = CI21[['Y','dist2GB','dist2TJ','schmid',
                'diff_schmid','misangle','eqStrain',
                'eqStress','act_pr','act_py','work']] 

elif args.var == 'set2':
    CI02 = CI02[['Y','dist2GB','dist2TJ','schmid','work','volratio_an']] 
    CI04 = CI04[['Y','dist2GB','dist2TJ','schmid','work','volratio_an']] 
    CI06 = CI06[['Y','dist2GB','dist2TJ','schmid','work','volratio_an']] 
    CI09 = CI09[['Y','dist2GB','dist2TJ','schmid','work','volratio_an']] 
    CI21 = CI21[['Y','dist2GB','dist2TJ','schmid','work','volratio_an']] 


# Test sets
X1 = CI02.loc[:,CI02.columns != 'Y'] 
X2 = CI04.loc[:,CI04.columns != 'Y'] 
X3 = CI06.loc[:,CI06.columns != 'Y'] 
X4 = CI09.loc[:,CI09.columns != 'Y'] 
X5 = CI21.loc[:,CI21.columns != 'Y'] 

y1 = CI02['Y']
y2 = CI04['Y']
y3 = CI06['Y']
y4 = CI09['Y']
y5 = CI21['Y']

names = ["CI02","CI04","CI06","CI09","CI21"]
vars = [X1,X2,X3,X4,X5]
group = [y1,y2,y3,y4,y5]

if args.sample == "all":
    # original sample
    train_names = names.copy()
    train_vars = vars.copy()
    train_group = group.copy()

elif args.sample == "under":
    # undersample
    C1_02 = CI02[CI02['Y']==1]
    C1_04 = CI04[CI04['Y']==1]
    C1_06 = CI06[CI06['Y']==1]
    C1_09 = CI09[CI09['Y']==1]
    C1_21 = CI21[CI21['Y']==1]

    C0_02 = CI02[CI02['Y']==0]
    C0_04 = CI04[CI04['Y']==0]
    C0_06 = CI06[CI06['Y']==0]
    C0_09 = CI09[CI09['Y']==0]
    C0_21 = CI21[CI21['Y']==0]

    nb_c1_02 = np.shape(C1_02)[0]
    nb_c1_04 = np.shape(C1_04)[0]
    nb_c1_06 = np.shape(C1_06)[0]
    nb_c1_09 = np.shape(C1_09)[0]
    nb_c1_21 = np.shape(C1_21)[0]

    C0_02_eq = C0_02.sample(nb_c1_02)
    C0_04_eq = C0_04.sample(nb_c1_04)
    C0_06_eq = C0_06.sample(nb_c1_06)
    C0_09_eq = C0_09.sample(nb_c1_09)
    C0_21_eq = C0_21.sample(nb_c1_21)

    CI02_US = pd.concat((C0_02_eq,C1_02))
    CI04_US = pd.concat((C0_04_eq,C1_04))
    CI06_US = pd.concat((C0_06_eq,C1_06))
    CI09_US = pd.concat((C0_09_eq,C1_09))
    CI21_US = pd.concat((C0_21_eq,C1_21))

    X1_US = CI02_US.loc[:,CI02_US.columns != 'Y'] 
    X2_US = CI04_US.loc[:,CI04_US.columns != 'Y'] 
    X3_US = CI06_US.loc[:,CI06_US.columns != 'Y'] 
    X4_US = CI09_US.loc[:,CI09_US.columns != 'Y'] 
    X5_US = CI21_US.loc[:,CI21_US.columns != 'Y'] 

    y1_US = CI02_US['Y']
    y2_US = CI04_US['Y']
    y3_US = CI06_US['Y']
    y4_US = CI09_US['Y']
    y5_US = CI21_US['Y']

    train_names = ["CI02_US","CI04_US","CI06_US","CI09_US","CI21_US"]
    train_vars = [X1_US,X2_US,X3_US,X4_US,X5_US]
    train_group = [y1_US,y2_US,y3_US,y4_US,y5_US]

elif args.sample == "over":
    # oversample
    C1_02 = CI02[CI02['Y']==1]
    C1_04 = CI04[CI04['Y']==1]
    C1_06 = CI06[CI06['Y']==1]
    C1_09 = CI09[CI09['Y']==1]
    C1_21 = CI21[CI21['Y']==1]

    C0_02 = CI02[CI02['Y']==0]
    C0_04 = CI04[CI04['Y']==0]
    C0_06 = CI06[CI06['Y']==0]
    C0_09 = CI09[CI09['Y']==0]
    C0_21 = CI21[CI21['Y']==0]

    nb_c1_02 = np.shape(C1_02)[0]
    nb_c1_04 = np.shape(C1_04)[0]
    nb_c1_06 = np.shape(C1_06)[0]
    nb_c1_09 = np.shape(C1_09)[0]
    nb_c1_21 = np.shape(C1_21)[0]

    nb_c0_02 = np.shape(C0_02)[0]
    nb_c0_04 = np.shape(C0_04)[0]
    nb_c0_06 = np.shape(C0_06)[0]
    nb_c0_09 = np.shape(C0_09)[0]
    nb_c0_21 = np.shape(C0_21)[0]

    C1_02_eq = C1_02.copy()
    for i in range((nb_c0_02 // nb_c1_02) -1):
        C1_02_eq = pd.concat((C1_02_eq,C1_02))
    C1_02_eq = pd.concat((C1_02_eq,C1_02.sample(nb_c0_02 % nb_c1_02)))

    C1_04_eq = C1_04.copy()
    for i in range((nb_c0_04 // nb_c1_04) -1):
        C1_04_eq = pd.concat((C1_04_eq,C1_04))
    C1_04_eq = pd.concat((C1_04_eq,C1_04.sample(nb_c0_04 % nb_c1_04)))

    C1_06_eq = C1_06.copy()
    for i in range((nb_c0_06 // nb_c1_06) -1):
        C1_06_eq = pd.concat((C1_06_eq,C1_06))
    C1_06_eq = pd.concat((C1_06_eq,C1_06.sample(nb_c0_06 % nb_c1_06)))

    C1_09_eq = C1_09.copy()
    for i in range((nb_c0_09 // nb_c1_09) -1):
        C1_09_eq = pd.concat((C1_09_eq,C1_09))
    C1_09_eq = pd.concat((C1_09_eq,C1_09.sample(nb_c0_09 % nb_c1_09)))

    C1_21_eq = C1_21.copy()
    for i in range((nb_c0_21 // nb_c1_21) -1):
        C1_21_eq = pd.concat((C1_21_eq,C1_21))
    C1_21_eq = pd.concat((C1_21_eq,C1_21.sample(nb_c0_21 % nb_c1_21)))

    CI02_OS = pd.concat((C0_02,C1_02_eq))
    CI04_OS = pd.concat((C0_04,C1_04_eq))
    CI06_OS = pd.concat((C0_06,C1_06_eq))
    CI09_OS = pd.concat((C0_09,C1_09_eq))
    CI21_OS = pd.concat((C0_21,C1_21_eq))

    X1_OS = CI02_OS.loc[:,CI02_OS.columns != 'Y'] 
    X2_OS = CI04_OS.loc[:,CI04_OS.columns != 'Y'] 
    X3_OS = CI06_OS.loc[:,CI06_OS.columns != 'Y'] 
    X4_OS = CI09_OS.loc[:,CI09_OS.columns != 'Y'] 
    X5_OS = CI21_OS.loc[:,CI21_OS.columns != 'Y'] 

    y1_OS = CI02_OS['Y']
    y2_OS = CI04_OS['Y']
    y3_OS = CI06_OS['Y']
    y4_OS = CI09_OS['Y']
    y5_OS = CI21_OS['Y']

    train_names = ["CI02_OS","CI04_OS","CI06_OS","CI09_OS","CI21_OS"]
    train_vars = [X1_OS,X2_OS,X3_OS,X4_OS,X5_OS]
    train_group = [y1_OS,y2_OS,y3_OS,y4_OS,y5_OS]

elif args.sample == "threshold":
     # threshold
    CI02_sub = CI02[(2*CI02["dist2GB"] + CI02["dist2TJ"] < 60)]
    CI04_sub = CI04[(2*CI04["dist2GB"] + CI04["dist2TJ"] < 60)]
    CI06_sub = CI06[(2*CI06["dist2GB"] + CI06["dist2TJ"] < 60)]
    CI09_sub = CI09[(2*CI09["dist2GB"] + CI09["dist2TJ"] < 60)]
    CI21_sub = CI21[(2*CI21["dist2GB"] + CI21["dist2TJ"] < 60)]

    # Spliting variables for train
    print("spliting variables for train")
    X1 = CI02_sub.loc[:,CI02_sub.columns != 'Y'] 
    X2 = CI04_sub.loc[:,CI04_sub.columns != 'Y'] 
    X3 = CI06_sub.loc[:,CI06_sub.columns != 'Y'] 
    X4 = CI09_sub.loc[:,CI09_sub.columns != 'Y'] 
    X5 = CI21_sub.loc[:,CI21_sub.columns != 'Y'] 

    y1 = CI02_sub['Y']
    y2 = CI04_sub['Y']
    y3 = CI06_sub['Y']
    y4 = CI09_sub['Y']
    y5 = CI21_sub['Y']

    train_names = ["CI02_sub","CI04_sub","CI06_sub","CI09_sub","CI21_sub"]
    train_vars = [X1,X2,X3,X4,X5]
    train_group = [y1,y2,y3,y4,y5]

acc = []
prec = []
recall = []
specif = []
confmat = []


# 
print("start learning")
for i in tqdm(range(len(vars))):
    ln = train_names.copy()
    ln.pop(i)
    lX = train_vars.copy()
    lX.pop(i)
    ly = train_group.copy()
    ly.pop(i)

    X_test = vars[i]
    y_test = group[i]

    for j in range(len(lX)):
        if j == 0:
            X_train = lX[j]
            y_train = ly[j]
        else:
            X_train = pd.concat([X_train,lX[j]])
            y_train = pd.concat([y_train,ly[j]])

    print("Learning :\n \tTrain : ",ln,"\n \tTest : ",names[i])

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    clf = cl
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    cm = metrics.confusion_matrix(y_test, y_pred)

    if mod == "kmeans":
        if (metrics.recall_score(y_test, y_pred) < (1-(cm[0,0] / (cm[0,0] + cm[0,1])))):
            y_pred = - (y_pred-1)

    np.save("pred/"+file_name+"-"+names[i],y_pred)

    if mod == "SVM":
        if probability :
            y_pred_prob = clf.predict_proba(X_test)
            np.save("pred/"+file_name+"-prob-"+names[i],y_pred_prob)

    acc.append(metrics.accuracy_score(y_test, y_pred))
    prec.append(metrics.precision_score(y_test, y_pred))
    recall.append(metrics.recall_score(y_test, y_pred))
    specif.append(cm[0,0] / (cm[0,0] + cm[0,1]))
    confmat.append(cm.flatten())


print("Learning done")
print("Saving results")

results = [acc,prec,recall,specif]

np.savetxt("res/" + file_name + "_res.txt",results)
np.savetxt("res/" + file_name + "_confmats.txt",np.array(confmat))



