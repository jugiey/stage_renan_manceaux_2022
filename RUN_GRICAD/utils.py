import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib import colors

from logging import error, warning


def load_data(file):
    """
    """
    
    data = np.load(file)

    for i in range(np.shape(data)[2]):
        c = data[:,:,i].flatten()
        if i == 0:
            df = c.T
        else:
            df = np.column_stack((df,c.T))

    if np.shape(data)[2] == 15:
        df = pd.DataFrame(df, columns=['Y', 'dist2GB', 'dist2TJ', 'schmid', 'diff_schmid',
                                   'misangle', 'eqStrain', 'eqStress', 'act_pr', 'act_py', 'work','relativ_an',
                                   'fractional_an','volratio_an','flatness_an'])

    elif np.shape(data)[2] == 11:
        df = pd.DataFrame(df, columns=['Y', 'dist2GB', 'dist2TJ', 'schmid', 'diff_schmid',
                                    'misangle', 'eqStrain', 'eqStress', 'act_pr', 'act_py', 'work'])
    
    else :
        error("Data shape error. Should be (m,n,11) or (m,n,15)")
        exit(1)
    
    return(df)

# ----------------------------------------------

def conver2xr(data,shape,name):
    """
    """

    ds = xr.Dataset()
    features = data.columns

    for i in features :
        ds[i] = xr.DataArray(np.reshape(np.array(data[i]),shape),dims=("y","x"))

    ds.attrs['name'] = name

    return ds




# ---------------------------------------------

def plot_pred_proj(y_pred, y_test, im_shape, y_train=""):
    """
    """

    if y_train is "":
        
        img_p = np.array(-(y_pred+2*np.array(y_test))).reshape(im_shape[0],im_shape[1])

        cmap = colors.ListedColormap(['green','red','blue','white'])
        bounds=[-3.5,-2.5,-1.5,-0.5,0.5]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        img = plt.imshow(img_p, interpolation='nearest', origin='lower',cmap=cmap,norm=norm)

        cbar = plt.colorbar(img, cmap=cmap, norm=norm, boundaries=bounds, ticks=[-3, -2, -1,0])

        cbar.set_ticklabels(["True positive","False negative","False positive","True negative"])

        plt.show()

    else:

        p = pd.DataFrame((np.array(y_test), -(y_pred+2*np.array(y_test)))).T
        p.columns = ['y_test', 'y_pred']
        p.index = y_test.index

        im_pred = pd.concat((y_train, p['y_pred']))
        im_pred = im_pred.sort_index()

        img_p = np.array(im_pred).reshape(im_shape[0], im_shape[1])

        cmap = colors.ListedColormap(
            ['green', 'red', 'blue', 'white', 'yellow'])
        bounds = [-3.5, -2.5, -1.5, -0.5, 0.5, 1.5]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        img = plt.imshow(img_p, interpolation='nearest',
                         origin='lower', cmap=cmap, norm=norm)

        cbar = plt.colorbar(img, cmap=cmap, norm=norm,
                            boundaries=bounds, ticks=[-3, -2, -1, 0, 1])

        cbar.set_ticklabels(["True positive", "False negative",
                            "False positive", "True negative", 'Train set positive'])

        plt.show()
