# Introduction

<hr>

&nbsp;&nbsp;&nbsp; The objective of this section is to make you discover the laboratory and the scientific context of this internship.
After that, we will present the main objectives of the research project.

<hr>

```{tableofcontents}
```
