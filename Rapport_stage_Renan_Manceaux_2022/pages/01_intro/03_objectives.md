# Objectives

&nbsp;&nbsp;&nbsp; Ice recrystallisation a an not well known phenomena and this machine learning study has for aim to help to identify factors and parameters
which influence the recrystallisation mechanism and help to explain the apparition of this mechanism.
This internship is driven by two main objectives :

- The first objective is to **find a predictive classification model** which is adapted and well fitting the experimental data.
The aim is to predict from a random columnar-grained ice sample before compression where will appear new grains resulting from
the dynamic recrystallisation. A probabilistic model can also be answering the need by giving a zone where probability to
get new grains is higher.
- After that, the second objective is to apply explainable AI principles to **extract importance of parameters** in the classification model
in order to make light on the variables which influence and regulate the dynamic recrystallisation in ice.
