# Data description

<hr>

&nbsp;&nbsp;&nbsp; The aim of this section is to detail the nature, sources and characteristics of data in use for machine learning
methods. Some of the variables are experimental data previously treated. Others have been added to the dataset for this
internship. We will also expose a statistical exploration of data to show what problems will appear next.

<hr>

```{tableofcontents}
```