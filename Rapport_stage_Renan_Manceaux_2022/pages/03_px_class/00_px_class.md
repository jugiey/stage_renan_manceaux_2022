(sec:pix_class)=

# Pixel classification

<hr>

&nbsp;&nbsp;&nbsp; After enlightening data, we will now present the results of the classification algorithms applied on the "pixel" dataset, comparing methods, explaining results and choose best model with the best parameters for this dataset. The python pipeline used to apply cross validation on these models is available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/workinprogress/machinelearning_ice/stage_renan_manceaux_2022/-/blob/main/RUN_GRICAD/Cross_val.py).

</hr>

```{tableofcontents}
```
