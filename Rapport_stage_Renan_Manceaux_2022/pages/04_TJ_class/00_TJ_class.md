(sec:TJ_class)=

# Triple junction classification

<hr>

&nbsp;&nbsp;&nbsp; In this section, we will expose results of the classification on triple junctions by classical classification algorithm and by Artificial Neural Network and compare to previous results. We will try to extract more information from these new datasets.

</hr>

```{tableofcontents}
```