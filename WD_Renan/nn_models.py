import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader

from sklearn.preprocessing import StandardScaler    
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import ConfusionMatrixDisplay
from tqdm.notebook import tqdm
import xarray as xr
from torchsummary import summary
from matplotlib import cm as cmap
import random

import utils

###################
## CLASS DATASET ##
###################

class FullDataset(Dataset):
  def __init__(self, dataset, transform_list=None):
    data_X = dataset[['schmid','diff_schmid','misangle','work','eqStrain','eqStress','act_pr','act_py'
                      ]].to_array().to_numpy().reshape((dataset.dims['nbtj'],8,10,10))
    data_X2 = dataset[['dist_to_1neigh','volratio_an','sum_pix_g']].to_array().to_numpy().reshape((dataset.dims['nbtj'],3))
    data_y = dataset['RX'].to_numpy()
    X_tensor, X2_tensor, y_tensor = torch.tensor(data_X), torch.tensor(data_X2),torch.tensor(data_y)
    tensors = (X_tensor, X2_tensor ,y_tensor)
    assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors)
    self.tensors = tensors
    self.transforms = transform_list

  def __getitem__(self, index):
    x = self.tensors[0][index]

    x2 = self.tensors[1][index]

    if self.transforms:
      #for transform in self.transforms: 
      #  x = transform(x)
      x = self.transforms(x)

    y = self.tensors[2][index]

    return x, x2 ,y

  def __len__(self):
    return self.tensors[0].size(0)

# ----------------------------------

class SmallDataset(Dataset):
  def __init__(self, dataset, transform_list=None):
    data_X = dataset[['schmid','work'
                      ]].to_array().to_numpy().reshape((dataset.dims['nbtj'],2,10,10))
    data_X2 = dataset['volratio_an'].to_numpy().reshape((dataset.dims['nbtj'],1))
    data_y = dataset['RX'].to_numpy()
    X_tensor, X2_tensor, y_tensor = torch.tensor(data_X), torch.tensor(data_X2),torch.tensor(data_y)
    tensors = (X_tensor, X2_tensor ,y_tensor)
    assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors)
    self.tensors = tensors
    self.transforms = transform_list

  def __getitem__(self, index):
    x = self.tensors[0][index]

    x2 = self.tensors[1][index]

    if self.transforms:
      #for transform in self.transforms: 
      #  x = transform(x)
      x = self.transforms(x)

    y = self.tensors[2][index]

    return x, x2 ,y

  def __len__(self):
    return self.tensors[0].size(0)

########################
## LEARNING FUNCTIONS ##
########################

def binary_acc(y_pred, y_test):
    y_pred_tag = torch.round(torch.sigmoid(y_pred))

    correct_results_sum = (y_pred_tag == y_test).sum().float()
    acc = correct_results_sum/y_test.shape[0]
    acc = torch.round(acc * 100)
    
    return acc

# ----------------------------------


def binary_rec(y_pred, y_test):
    y_pred_tag = torch.round(torch.sigmoid(y_pred))

    cm = torch.zeros(2, 2)
    for t, p in zip(y_test.long(), y_pred_tag.long()):
        cm[t, p] += 1
    rec = cm[1,1] / (cm[1,1] + cm[1,0])
    rec = torch.round(rec * 100)
    
    return rec

# ----------------------------------


def binary_spec(y_pred, y_test):
    y_pred_tag = torch.round(torch.sigmoid(y_pred))
    
    cm = torch.zeros(2, 2)
    for t, p in zip(y_test.long(), y_pred_tag.long()):
        cm[t, p] += 1

    spec = cm[0,0] / (cm[0,0] + cm[0,1])
    spec = torch.round(spec * 100)
    
    return spec

#############################
## UNDERSAMPLING FUNCTIONS ##
#############################

def us(data_list):
    """
    """

    train = xr.concat(data_list,dim='nbtj')

    dt0 = train.where(train.RX==0).dropna(dim='nbtj')
    dt0 = dt0.drop('coords')
    dt1 = train.where(train.RX==1).dropna(dim='nbtj')
    dt1 = dt1.drop('coords')
    n0 = dt0.dims['nbtj']
    n1 = dt1.dims['nbtj']

    dt0u = utils.xr_sample(dt0,n1)

    train = xr.concat((dt0u,dt1),dim='nbtj')
    return train

# ----------------------------------

def usadj(data_list):
    """
    """

    first = True
    for dt in data_list:
        dt0 = dt.where(dt.RX==0).dropna(dim='nbtj')
        dt0 = dt0.drop('coords')
        dt1 = dt.where(dt.RX==1).dropna(dim='nbtj')
        dt1 = dt1.drop('coords')
        n0 = dt0.dims['nbtj']
        n1 = dt1.dims['nbtj']
        nn0 = n0
        nn1 = n1
        # balance 0
        if  n0 > 100:
            dt0 = utils.xr_sample(dt0,100)
            nn0 = 100
        else :
            f0 = utils.flip(dt0)
            if n0 == min(100-n0,n0):
                add = f0            
            else:
                add = utils.xr_sample(f0,100-n0)
            nn0 = nn0 + min(100-n0,n0)
            if nn0 < 100:
                r0 = utils.rot180(dt0)
                if n0 == min(100-nn0,n0):
                    add = xr.concat((add,r0),dim='nbtj')            
                else:
                    add = xr.concat((add,utils.xr_sample(r0,100-nn0)),dim='nbtj')
                nn0 = nn0 + min(100-nn0,n0)
                if nn0 < 100:
                    rf0 = utils.flip(utils.rot180(dt0))
                    if n0 == min(100-nn0,n0):
                        add = xr.concat((add,rf0),dim='nbtj')            
                    else:
                        add = xr.concat((add,utils.xr_sample(rf0,100-nn0)),dim='nbtj')
                    nn0 = nn0 + min(100-nn0,n0)
            dt0 = xr.concat((dt0,add),dim='nbtj')
        # balance 1
        if  n1 > 100:
            dt1 = utils.xr_sample(dt1,100)
            nn1 = 100
        else :
            f1 = utils.flip(dt1)
            if n1 == min(100-n1,n1):
                add = f1            
            else:
                add = utils.xr_sample(f1,100-n1)
            nn1 = nn1 + min(100-n1,n1)
            if nn1 < 100:
                r1 = utils.rot180(dt1)
                if n1 == min(100-nn1,n1):
                    add = xr.concat((add,r1),dim='nbtj')            
                else:
                    add = xr.concat((add,utils.xr_sample(r1,100-nn1)),dim='nbtj')
                nn1 = nn1 + min(100-nn1,n1)
                if nn1 < 100:
                    rf1 = utils.flip(utils.rot180(dt1))
                    if n1 == min(100-nn1,n1):
                        add = xr.concat((add,rf1),dim='nbtj')            
                    else:
                        add = xr.concat((add,utils.xr_sample(rf1,100-nn1)),dim='nbtj')
                    nn1 = nn1 + min(100-nn1,n1)
                    if nn1 < 100:
                        p = utils.xr_sample(add,100-nn1)
                        add = xr.concat((add,p),dim='nbtj')
            dt1 = xr.concat((dt1,add),dim='nbtj')
        if first:
            train = xr.concat((dt0,dt1),dim='nbtj')
            first=False
        else :
            train = xr.concat((train,dt0,dt1),dim='nbtj')

    return train


def norm(dl):
    """
    Normalization function

    :param dl: datasets to normalize together
    :type dl: list of xr.dataset

    Return :
        - list : normalized datasets 
    """

    data_list = dl.copy()

    dt = xr.concat(data_list,dim='nbtj')

    vars = list(dt.keys())
    vars_m = vars[1:9]
    vars_s = vars[10:13]
    for i in vars_m :
        vt = dt[i].to_numpy().reshape((dt.dims['nbtj'],100))
        scaler = StandardScaler().fit(vt)
        for j in range(len(data_list)):
            v = data_list[j][i].to_numpy().reshape((data_list[j].dims['nbtj'],100))
            v_scaled = scaler.transform(v)
            v_scaled = v_scaled.reshape((data_list[j].dims['nbtj'],10,10))
            data_list[j][i] = xr.DataArray(v_scaled,dims=['nbtj','y','x'])
    for i in  vars_s :
        vt = dt[i].to_numpy()
        for j in range(len(data_list)):
            v = data_list[j][i].to_numpy()
            v_scaled = (v - vt.mean())/vt.std()
            data_list[j][i] = xr.DataArray(v_scaled,dims=['nbtj'])
    
    return data_list

###############
## NN MODElS ##
###############

class CNet1(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=64, kernel_size=3)
        self.conv2 = self.conv_block(c_in=64, c_out=128, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout2d(0.1)
        self.dense = nn.Linear(128, 1)
        self.prelu = nn.PReLU()

    def forward(self, x, x2):
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        x = self.prelu(self.dense(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net1(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=64, kernel_size=3)
        self.conv2 = self.conv_block(c_in=64, c_out=128, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout2d(0.1)
        self.dense1 = nn.Linear(3, 128)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(256, 128)
        self.dense3 = nn.Linear(128,1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.prelu(self.dense2(x))
        x = self.prelu(self.dense3(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net2(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=16, kernel_size=3)
        self.conv2 = self.conv_block(c_in=16, c_out=32, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.1)
        self.dense1 = nn.Linear(3, 32)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(64, 1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.prelu(self.dense2(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net3(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=16, kernel_size=3)
        self.conv2 = self.conv_block(c_in=16, c_out=32, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.1)
        self.dense1 = nn.Linear(3, 32)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(64, 1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.dropout(x)
        x = self.prelu(self.dense2(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net4(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=16, kernel_size=3)
        self.conv2 = self.conv_block(c_in=16, c_out=32, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.1)
        self.dense1 = nn.Linear(3, 32)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(64, 32)
        self.dense3 = nn.Linear(32, 1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.dropout(x)
        x = self.prelu(self.dense2(x))
        x = self.dropout(x)
        x = self.prelu(self.dense3(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net5(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=32, kernel_size=3)
        self.conv2 = self.conv_block(c_in=32, c_out=64, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.1)
        self.dense1 = nn.Linear(3, 64)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(128, 64)
        self.dense3 = nn.Linear(64, 1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.dropout(x)
        x = self.prelu(self.dense2(x))
        x = self.dropout(x)
        x = self.prelu(self.dense3(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net6(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=128, kernel_size=3)
        self.conv2 = self.conv_block(c_in=128, c_out=256, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.3)
        self.dense1 = nn.Linear(3, 128)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(256 + 128, 64)
        self.dense3 = nn.Linear(64, 1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.dropout(x)
        x = self.prelu(self.dense2(x))
        x = self.dropout(x)
        x = self.prelu(self.dense3(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net_min(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=8, c_out=16, kernel_size=3)
        self.conv2 = self.conv_block(c_in=16, c_out=32, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout2d(0.1)
        self.prelu = nn.PReLU()
        self.dense = nn.Linear(35,1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x = torch.cat((x,x2),1)
        x = self.prelu(self.dense(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block

# ----------------------------------

class Net1_Small(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = self.conv_block(c_in=2, c_out=64, kernel_size=3)
        self.conv2 = self.conv_block(c_in=64, c_out=128, kernel_size=3)
        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout2d(0.1)
        self.dense1 = nn.Linear(1, 128)
        self.prelu = nn.PReLU()
        self.dense2 = nn.Linear(256, 128)
        self.dense3 = nn.Linear(128,1)

    def forward(self, x , x2):
        #CNN
        x = self.pool(self.conv1(x))
        x = self.pool(self.conv2(x))
        x = x.reshape((x.shape[0],x.shape[1]))
        #DNN
        x2 = self.prelu(self.dense1(x2))
        x = torch.cat((x,x2),1)
        x = self.prelu(self.dense2(x))
        x = self.prelu(self.dense3(x))
        return x

    def conv_block(self, c_in, c_out,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.PReLU()
        )        
        return seq_block


