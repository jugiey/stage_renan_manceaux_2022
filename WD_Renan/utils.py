import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib import cm
import random

from logging import error, warning

#######################
## LOADING FUNCTIONS ##
#######################

def load_data(file):
    """
    Loading function for prepared data for ML stored in npy .file

    :param file: path to data
    :type file: string

    Return :
        - pandas.Dataframe : reorganized data with variables in columns and pixels in rows
    """
    
    data = np.load(file)

    for i in range(np.shape(data)[2]):
        c = data[:,:,i].flatten()
        if i == 0:
            df = c.T
        else:
            df = np.column_stack((df,c.T))

    if np.shape(data)[2] == 15:
        df = pd.DataFrame(df, columns=['Y', 'dist2GB', 'dist2TJ', 'schmid', 'diff_schmid',
                                   'misangle', 'eqStrain', 'eqStress', 'act_pr', 'act_py', 'work','relativ_an',
                                   'fractional_an','volratio_an','flatness_an'])

    elif np.shape(data)[2] == 11:
        df = pd.DataFrame(df, columns=['Y', 'dist2GB', 'dist2TJ', 'schmid', 'diff_schmid',
                                    'misangle', 'eqStrain', 'eqStress', 'act_pr', 'act_py', 'work'])
    
    else :
        error("Data shape error. Should be (m,n,11) or (m,n,15)")
        exit(1)

    df.Y = df.Y.values.astype(int)

    return(df)

# ----------------------------------------------

def conver2xr(data,shape,name):
    """
    Convert pandas.dataframe into xarray

    :param data: initial dataframe
    :type data: pandas.Dataframe
    :param shape: shape of data
    :type shape: tuple of int
    :param name: name of the dataset
    :type name: string

    Return :
        - xarray.Dataset : Converted dataset

    """

    ds = xr.Dataset()
    features = data.columns

    for i in features :
        ds[i] = xr.DataArray(np.reshape(np.array(data[i]),shape),dims=("y","x"))

    ds.attrs['name'] = name

    return ds

# ----------------------------------------------

def load_tj_data(file):
    """
    Load dataset of TJ of 19 variables

    :param file: path to data
    :type file: string
    """

    data = np.load(file)

    df = pd.DataFrame(data,columns=["RX","schmid1","schmid2","schmid3","diff_schmid1","diff_schmid2","diff_schmid3","misangle1","misangle2","misangle3",
        "relative_an","eqStrain","eqStress","act_pr","act_py","work","dist1neigh","nb_pix_g1","nb_pix_g2","nb_pix_g3"])

    return df

#######################
## PLOTING FUNCTIONS ##
#######################

def plot_pred_proj(y_pred, y_test, im_shape, y_train=np.array([]),TJ=False):
    """
    Ploting function for projection of prediction. 
    y_pred and y_test or y_pred and y_train+y_test must be of same size

    :param y_pred: vector of predicted labels
    :type y_pred: array
    :param y_test: vector of true labels
    :type y_test: array
    :param im_shape: shape of the testing image before flatting
    :type im_shape: (int,int)
    :param y_train: if given function will plot train set in diffent color. y_train and y_test must be a completed picture. 
    :type y_train: array

    """

    if y_train.size==0:
        
        img_p = np.array(-(y_pred+2*np.array(y_test))).reshape(im_shape[0],im_shape[1])

        cmap = colors.ListedColormap(['green','red','blue','white'])
        bounds=[-3.5,-2.5,-1.5,-0.5,0.5]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        img = plt.imshow(img_p, interpolation='nearest', origin='lower',cmap=cmap,norm=norm)

        cbar = plt.colorbar(img, cmap=cmap, norm=norm, boundaries=bounds, ticks=[-3, -2, -1,0])

        cbar.set_ticklabels(["True positive","False negative","False positive","True negative"])

        plt.show()

    else:

        p = pd.DataFrame((np.array(y_test), -(y_pred+2*np.array(y_test)))).T
        p.columns = ['y_test', 'y_pred']
        p.index = y_test.index

        im_pred = pd.concat((y_train, p['y_pred']))
        im_pred = im_pred.sort_index()

        img_p = np.array(im_pred).reshape(im_shape[0], im_shape[1])

        if TJ :
            cmap = colors.ListedColormap(
                ['green', 'red', 'blue', 'gray', 'yellow'])
        else:
            cmap = colors.ListedColormap(
                ['green', 'red', 'blue', 'white', 'yellow'])
        bounds = [-3.5, -2.5, -1.5, -0.5, 0.5, 1.5]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        img = plt.imshow(img_p, interpolation='nearest',
                         origin='lower', cmap=cmap, norm=norm)

        cbar = plt.colorbar(img, cmap=cmap, norm=norm,
                            boundaries=bounds, ticks=[-3, -2, -1, 0, 1])

        cbar.set_ticklabels(["True positive", "False negative",
                            "False positive", "True negative", 'Train set positive'])

        plt.show()

# ----------------------------------------------

def plot_res_CV(file,bar=True,ROC=True,Rcurve=True):
    """
    Ploting function for results of cross validation.

    :param file: path to results.txt file made by np.savetxt on an array of 4 rows (Accuracy,Precision,Recall,Specificity)
    :type file: string
    :param bar: plot a bar plot with std of each statistic
    :type bar: bool
    :param ROC: plot each learning on ROC curve 
    :type ROC: bool
    :param Rcurve: plot the curve between points
    :type Rcurve: bool

    """
    f = file.split('/')[-1]
    name = f.split('_')[1]

    res = np.loadtxt(file)
    
    Accuracy = res[0]
    Precision = res[1]
    Recall = res[2]
    specif = res[3]

    moyAcc = np.mean(Accuracy)
    moyPre = np.mean(Precision)
    moyRec = np.mean(Recall)
    moyspecif = np.mean(specif)

    sdAcc = np.std(Accuracy)
    sdPre = np.std(Precision)
    sdRec = np.std(Recall)
    sdspecif = np.std(specif)

    Train_stat = ['Accuracy', 'Precision', 'Recall','Specificity']
    x_pos = np.arange(len(Train_stat))
    moy = [moyAcc, moyPre, moyRec, moyspecif]
    sd = [sdAcc, sdPre, sdRec, sdspecif]

    if (bar & ROC):
        fig, ax = plt.subplots(nrows=1,ncols=2,figsize=(15,5))
        ax[0].bar(x_pos, moy, yerr=sd, align='center', alpha=0.5, ecolor='blue', capsize=0.1)
        ax[0].set_ylabel('Values (%)')
        ax[0].set_xticks(x_pos)
        ax[0].set_xticklabels(Train_stat)
        ax[0].set_title('Train stats',fontweight='bold')
        ax[0].yaxis.grid(True)
        ax[0].set_ylim(0,1.1)

        for i, v in enumerate(moy):
            ax[0].text(i - 0.1 , 1.05, round(v,2), color='blue', fontweight='bold')


        plt.plot(1-specif,Recall,'+b')
        plt.plot([0,1],[0,1],"black")
        
        if Rcurve:
            xc = np.append(np.insert(1-specif,0,0),1)
            yc = np.append(np.insert(Recall,0,0),1)
            yc = [y for _, y in sorted(zip(xc, yc))]
            xc = np.sort(xc)
            plt.plot(xc,yc)
            
        plt.xlim(0,1)
        plt.ylim(0,1)
        ax[1].set_title('ROC',fontweight='bold')
        ax[1].set_ylabel('Recall')
        ax[1].set_xlabel('1 - Specificity')

        plt.suptitle(name + ' cross validation results',fontsize=20)
    
    elif bar :
        fig, ax = plt.subplots(figsize=(8,5))
        ax.bar(x_pos, moy, yerr=sd, align='center', alpha=0.5, ecolor='blue', capsize=0.1)
        ax.set_ylabel('Values (%)')
        ax.set_xticks(x_pos)
        ax.set_xticklabels(Train_stat)
        ax.set_title('Train stats',fontweight='bold')
        ax.yaxis.grid(True)
        ax.set_ylim(0,1.1)

        for i, v in enumerate(moy):
            ax.text(i - 0.1 , 1.05, round(v,2), color='blue', fontweight='bold')

        plt.suptitle(name + ' cross validation results',fontsize=20)

    elif ROC:
        fig, ax = plt.subplots(figsize=(8,5))
        plt.plot(1-specif,Recall,'+b')
        plt.plot([0,1],[0,1],"black")

        if Rcurve:
            xc = np.append(np.insert(1-specif,0,0),1)
            yc = np.append(np.insert(Recall,0,0),1)
            yc = [y for _, y in sorted(zip(xc, yc))]
            xc = np.sort(xc)
            plt.plot(xc,yc)

        plt.xlim(0,1)
        plt.ylim(0,1)
        ax.set_title('ROC',fontweight='bold')
        ax.set_ylabel('Recall')
        ax.set_xlabel('1 - Specificity')

        plt.suptitle(name + ' cross validation results',fontsize=20)

# ----------------------------------------------

def plot_lstats(acc,prec,rec,spec,model="",bar=True,ROC=True,Rcurve=False):
    """
    Ploting function for learning statistics.

    :param acc: Accuracy score
    :param acc: float
    :param prec: Precision score
    :param prec: float
    :param rec: Recall score
    :param rec: float
    :param spec: Specificity score
    :param spec: float
    :param model: Name of model for caption
    :param model: string

    :param bar: plot a bar plot with std of each statistic
    :type bar: bool
    :param ROC: plot each learning on ROC curve 
    :type ROC: bool
    :param Rcurve: plot the curve between points
    :type Rcurve: bool
    """
        
    Train_stat = ['Accuracy', 'Precision', 'Recall','Specificity']
    x_pos = np.arange(len(Train_stat))
    val = [acc, prec, rec, spec]

    if (bar & ROC):
        fig, ax = plt.subplots(nrows=1,ncols=2,figsize=(15,5))
        ax[0].bar(x_pos, val, align='center', alpha=0.5, ecolor='blue', capsize=0.1)
        ax[0].set_ylabel('Values (%)')
        ax[0].set_xticks(x_pos)
        ax[0].set_xticklabels(Train_stat)
        ax[0].set_title('Train stats',fontweight='bold')
        ax[0].yaxis.grid(True)
        ax[0].set_ylim(0,1.1)

        for i, v in enumerate(val):
            ax[0].text(i - 0.1 , 1.05, round(v,2), color='blue', fontweight='bold')


        plt.plot(1-spec,rec,'+b')
        plt.plot([0,1],[0,1],"black")
        
        if Rcurve:
            xc = np.append(np.insert(1-spec,0,0),1)
            yc = np.append(np.insert(rec,0,0),1)
            yc = [y for _, y in sorted(zip(xc, yc))]
            xc = np.sort(xc)
            plt.plot(xc,yc)
            
        plt.xlim(0,1)
        plt.ylim(0,1)
        ax[1].set_title('ROC',fontweight='bold')
        ax[1].set_ylabel('Recall')
        ax[1].set_xlabel('1 - Specificity')

        plt.suptitle(model + ' results',fontsize=20)
    
    elif bar :
        fig, ax = plt.subplots(figsize=(8,5))
        ax.bar(x_pos, val, align='center', alpha=0.5, ecolor='blue', capsize=0.1)
        ax.set_ylabel('Values (%)')
        ax.set_xticks(x_pos)
        ax.set_xticklabels(Train_stat)
        ax.set_title('Train stats',fontweight='bold')
        ax.yaxis.grid(True)
        ax.set_ylim(0,1.1)

        for i, v in enumerate(val):
            ax.text(i - 0.1 , 1.05, round(v,2), color='blue', fontweight='bold')

        plt.suptitle(model + ' results',fontsize=20)

    elif ROC:
        fig, ax = plt.subplots(figsize=(8,5))
        plt.plot(1-spec,rec,'+b')
        plt.plot([0,1],[0,1],"black")

        if Rcurve:
            xc = np.append(np.insert(1-spec,0,0),1)
            yc = np.append(np.insert(rec,0,0),1)
            yc = [y for _, y in sorted(zip(xc, yc))]
            xc = np.sort(xc)
            plt.plot(xc,yc)

        plt.xlim(0,1)
        plt.ylim(0,1)
        ax.set_title('ROC',fontweight='bold')
        ax.set_ylabel('Recall')
        ax.set_xlabel('1 - Specificity')

        plt.suptitle(model + ' results',fontsize=20)

# ----------------------------------------------

def ROC_patch(files):
    """
    Plotting function for ROC curve of several results files (from CV)

    :param files: list of res files path
    :type files: list of string
    """

    m_rec = []
    m_aspec = []
    sd_rec = []
    sd_aspec = []
    names = []

    for f in files:
        res = np.loadtxt(f)
        m_rec.append(np.mean(res[2]))
        m_aspec.append(np.mean(1-res[3]))

        sd_rec.append(np.std(res[2]))
        sd_aspec.append(np.std(1-res[3]))

        if f.split('/')[-1].split('_')[0]=="CV":
            names.append(f.split('/')[-1].split('_')[1])
        else:
            names.append(f.split('/')[-1].split('_')[0])

    x, y = np.array(m_aspec), m_rec

    colormap = cm.get_cmap('gist_rainbow', len(m_rec)) 


    fig, ax = plt.subplots(figsize=(10,10))

    handles = []

    for e in range(len(m_rec)):
        u=x[e]     
        v=y[e]    
        ax.scatter(u,v,color=colors.rgb2hex(colormap(e)),marker="+")
        a=sd_aspec[e]     
        b=sd_rec[e]    

        t = np.linspace(0, 2*np.pi, 100)
        ax.plot( u+a*np.cos(t) , v+b*np.sin(t) ,color=colors.rgb2hex(colormap(e)))

        handles.append(mpatches.Patch(color=colors.rgb2hex(colormap(e)), label=names[e]))


    legend = ax.legend(handles=handles , loc = "lower right",title="Model")
    ax.add_artist(legend)

    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    ax.plot([0,1],[0,1],"black")

    ax.set_title('ROC of models cross validation',fontweight='bold')
    ax.set_ylabel('Recall')
    ax.set_xlabel('1 - Specificity')

    return fig,ax

# ----------------------------------------------

def plot_m_sd(res,c=1):
    """
    Plot results mean and std  

    :param res: results values
    :type res: array
    :param c: color (1 or 2)
    :type c: integer
    """
    if c == 1:
        c1 = 'b'
        c2 = 'c'
    elif c == 2:
        c1 = 'r'
        c2 = 'm'
    else :
        print("c sould be 1 or 2")
        exit(1)

    res_m = np.nanmean(np.array(res),axis=0)
    res_sd = np.nanstd(np.array(res),axis=0)

    plt.plot(res_m,c1)
    plt.plot(res_m-res_sd, c2, linestyle='--')
    plt.plot(res_m+res_sd, c2, linestyle='--')

    plt.fill_between(np.arange(len(res_m)),res_m-res_sd,res_m+res_sd,color=c2,alpha=0.5)

# ----------------------------------------------

def plot_ls_NN(files):
    """
    Plot learning statistics of train test + ROC curve with convergence color scale

    :param files: paths to results files
    :type files: list
    """

    train_loss = []
    test_loss = []
    train_acc = []
    train_rec = []
    train_spec = []
    test_acc = []
    test_rec = []
    test_spec = []

    for f in files:
        res = np.loadtxt(f)
        train_loss.append(res[0])
        test_loss.append(res[1])
        train_acc.append(res[2])
        train_rec.append(res[3])
        train_spec.append(res[4])
        test_acc.append(res[5])
        test_rec.append(res[6])
        test_spec.append(res[7])


    plt.figure(figsize=(10,10))
    plt.subplot(221)

    plot_m_sd(train_loss,1)
    plot_m_sd(test_loss,2)

    line1 = Line2D([0,1],[0,1],linestyle='-', color='b')
    line2 = Line2D([0,1],[0,1],linestyle='--', color='c')
    line3 = Line2D([0,1],[0,1],linestyle='-', color='r')
    line4 = Line2D([0,1],[0,1],linestyle='--', color='m')

    plt.legend([line1,line2,line3,line4],['mean train','std train','mean test','std test'],loc='best')

    #plt.ylim(0,10)
    plt.ylim(0,np.max([train_loss,test_loss]))
    plt.title("Loss")
    plt.xlabel("Epochs")
    plt.ylabel("loss")

    plt.subplot(222)

    plot_m_sd(train_rec,1)
    plot_m_sd(np.array(test_rec)*100,2)

    line1 = Line2D([0,1],[0,1],linestyle='-', color='b')
    line2 = Line2D([0,1],[0,1],linestyle='--', color='c')
    line3 = Line2D([0,1],[0,1],linestyle='-', color='r')
    line4 = Line2D([0,1],[0,1],linestyle='--', color='m')

    plt.legend([line1,line2,line3,line4],['mean train','std train','mean test','std test'],loc='best')

    plt.ylim(0,100)
    plt.title("Recall")
    plt.xlabel("Epochs")
    plt.ylabel("recall")

    plt.subplot(223)

    plot_m_sd(train_spec,1)
    plot_m_sd(np.array(test_spec)*100,2)

    line1 = Line2D([0,1],[0,1],linestyle='-', color='b')
    line2 = Line2D([0,1],[0,1],linestyle='--', color='c')
    line3 = Line2D([0,1],[0,1],linestyle='-', color='r')
    line4 = Line2D([0,1],[0,1],linestyle='--', color='m')

    plt.legend([line1,line2,line3,line4],['mean train','std train','mean test','std test'],loc='best')

    plt.ylim(0,100)
    plt.title("Specif")
    plt.xlabel("Epochs")
    plt.ylabel("specif")

    plt.subplot(224)

    plot_m_sd(train_acc,1)
    plot_m_sd(np.array(test_acc)*100,2)

    line1 = Line2D([0,1],[0,1],linestyle='-', color='b')
    line2 = Line2D([0,1],[0,1],linestyle='--', color='c')
    line3 = Line2D([0,1],[0,1],linestyle='-', color='r')
    line4 = Line2D([0,1],[0,1],linestyle='--', color='m')

    plt.legend([line1,line2,line3,line4],['mean train','std train','mean test','std test'],loc='best')

    plt.ylim(0,100)
    plt.title("Accuracy")
    plt.xlabel("Epochs")
    plt.ylabel("accuracy")

    nl = f.split('/')[-1].split('_')
    mod = nl[0]
    if nl[2] != 'TJ':
        us = nl[2]
    else:
        us = ""

    lr = nl[-4].split('r')[1]
    b = nl[-2].split('b')[1]

    plt.suptitle("CV "+mod+" "+us+" LR = "+lr+" Batch_size = "+b,fontsize=16)

    plt.show()

    from matplotlib import cm as cmap
    colormap = cmap.coolwarm
    plt.figure(figsize=(7,6))

    for i in range(len(test_rec)):
        plt.scatter(1-np.array(test_spec[i]),np.array(test_rec[i]),c=np.arange(len(test_rec[0])),cmap=colormap)
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.plot([0,1],[0,1],"k")
    cb = plt.colorbar()
    cb.set_label('epochs')
    plt.xlabel('1 - specif')
    plt.ylabel('recall')
    plt.show()

##############################
## DATA TRANSFORM FUNCTIONS ##
##############################

def flip(data):
    """
    Apply flip left right to all mapped variables of CNN data 
    Run data CNN pipeline before using this function to dataset (data variables format and attributes)

    :param data: CNN data 
    :type data: xr.Dataset

    Return :
        - xr.Dataset : fliped dataset

    """

    def flip_arr_o_mat(arr):
        """
        Flip left right all elements of array of matrix 2x2 without changing index

        :param arr: array of matrix (nb,y,x)
        :type arr: xr.DataArray

        Return :
            - xr.DataArray : fliped array of matrix (nb,y,x)

        """
        fliped = np.zeros(arr.shape)
        for i in range(arr.shape[0]):
            fliped[i] = np.fliplr(arr.to_numpy()[i])
        return xr.DataArray(fliped,dims=arr.dims)

    ds = xr.Dataset()

    RX = data.RX
    RX_map = flip_arr_o_mat(data.RX_map)
    schmid = flip_arr_o_mat(data.schmid)
    diff_schmid = flip_arr_o_mat(data.diff_schmid)
    misangle = flip_arr_o_mat(data.misangle)
    work = flip_arr_o_mat(data.work)
    eqStrain = flip_arr_o_mat(data.eqStrain)
    eqStress = flip_arr_o_mat(data.eqStress)
    act_pr = flip_arr_o_mat(data.act_pr)
    act_py = flip_arr_o_mat(data.act_py)
    dist2oTJ = data.dist_to_1neigh
    volratio_an = data.volratio_an
    nb_pix_g = data.sum_pix_g


    ds['RX'] = xr.DataArray(RX,dims="nbtj")
    ds['RX_map'] = xr.DataArray(RX_map,dims=["nbtj","y","x"])
    ds['schmid'] = xr.DataArray(schmid,dims=["nbtj","y","x"])
    ds['diff_schmid'] = xr.DataArray(diff_schmid,dims=["nbtj","y","x"])
    ds['misangle'] = xr.DataArray(misangle,dims=["nbtj","y","x"])
    ds['work'] = xr.DataArray(work,dims=["nbtj","y","x"])
    ds['eqStrain'] = xr.DataArray(eqStrain,dims=["nbtj","y","x"])
    ds['eqStress'] = xr.DataArray(eqStress,dims=["nbtj","y","x"])
    ds['act_pr'] = xr.DataArray(act_pr,dims=["nbtj","y","x"])
    ds['act_py'] = xr.DataArray(act_py,dims=["nbtj","y","x"])


    ds['dist_to_1neigh'] = xr.DataArray(dist2oTJ,dims='nbtj')
    ds['volratio_an'] = xr.DataArray(volratio_an,dims='nbtj')
    ds['sum_pix_g'] = xr.DataArray(nb_pix_g,dims='nbtj')

    return ds

# ----------------------------------------------

def rot180(data):
    """
    Apply 180° rotation to all mapped variables of CNN data 
    Run data CNN pipeline before using this function to dataset (data variables format and attributes)

    :param data: CNN data 
    :type data: xr.Dataset

    Return :
        - xr.Dataset : rotated dataset

    """

    def rot180_arr_o_mat(arr):
        """
        Rotate at 180° all elements of array of matrix 2x2 without changing index

        :param arr: array of matrix (nb,y,x)
        :type arr: xr.DataArray

        Return :
            - xr.DataArray : rotated array of matrix (nb,y,x)
        """
        rotated = np.zeros(arr.shape)
        for i in range(arr.shape[0]):
            rotated[i] = np.rot90(np.rot90(arr.to_numpy()[i]))
        return xr.DataArray(rotated,dims=arr.dims)

    ds = xr.Dataset()

    RX = data.RX
    RX_map = rot180_arr_o_mat(data.RX_map)
    schmid = rot180_arr_o_mat(data.schmid)
    diff_schmid = rot180_arr_o_mat(data.diff_schmid)
    misangle = rot180_arr_o_mat(data.misangle)
    work = rot180_arr_o_mat(data.work)
    eqStrain = rot180_arr_o_mat(data.eqStrain)
    eqStress = rot180_arr_o_mat(data.eqStress)
    act_pr = rot180_arr_o_mat(data.act_pr)
    act_py = rot180_arr_o_mat(data.act_py)
    dist2oTJ = data.dist_to_1neigh
    volratio_an = data.volratio_an
    nb_pix_g = data.sum_pix_g


    ds['RX'] = xr.DataArray(RX,dims="nbtj")
    ds['RX_map'] = xr.DataArray(RX_map,dims=["nbtj","y","x"])
    ds['schmid'] = xr.DataArray(schmid,dims=["nbtj","y","x"])
    ds['diff_schmid'] = xr.DataArray(diff_schmid,dims=["nbtj","y","x"])
    ds['misangle'] = xr.DataArray(misangle,dims=["nbtj","y","x"])
    ds['work'] = xr.DataArray(work,dims=["nbtj","y","x"])
    ds['eqStrain'] = xr.DataArray(eqStrain,dims=["nbtj","y","x"])
    ds['eqStress'] = xr.DataArray(eqStress,dims=["nbtj","y","x"])
    ds['act_pr'] = xr.DataArray(act_pr,dims=["nbtj","y","x"])
    ds['act_py'] = xr.DataArray(act_py,dims=["nbtj","y","x"])


    ds['dist_to_1neigh'] = xr.DataArray(dist2oTJ,dims='nbtj')
    ds['volratio_an'] = xr.DataArray(volratio_an,dims='nbtj')
    ds['sum_pix_g'] = xr.DataArray(nb_pix_g,dims='nbtj')

    return ds
    
# ----------------------------------------------

def xr_sample(dt,n):
    """
    Sample n TJ from CNN shaped dataset 
    Run data CNN pipeline before using this function to dataset (data variables format and attributes)

    :param dt: CNN data 
    :type dt: xr.Dataset

    Return :
        - xr.Dataset : samples of dt
    """
    rind = random.sample(range(0, dt.dims['nbtj']-1), n)
    ds = xr.Dataset()

    RX = dt.RX[rind]
    RX_map = dt.RX_map[rind]
    schmid = dt.schmid[rind]
    diff_schmid = dt.diff_schmid[rind]
    misangle = dt.misangle[rind]
    work = dt.work[rind]
    eqStrain = dt.eqStrain[rind]
    eqStress = dt.eqStress[rind]
    act_pr = dt.act_pr[rind]
    act_py = dt.act_py[rind]
    dist2oTJ = dt.dist_to_1neigh[rind]
    volratio_an = dt.volratio_an[rind]
    nb_pix_g = dt.sum_pix_g[rind]


    ds['RX'] = xr.DataArray(RX,dims="nbtj")
    ds['RX_map'] = xr.DataArray(RX_map,dims=["nbtj","y","x"])
    ds['schmid'] = xr.DataArray(schmid,dims=["nbtj","y","x"])
    ds['diff_schmid'] = xr.DataArray(diff_schmid,dims=["nbtj","y","x"])
    ds['misangle'] = xr.DataArray(misangle,dims=["nbtj","y","x"])
    ds['work'] = xr.DataArray(work,dims=["nbtj","y","x"])
    ds['eqStrain'] = xr.DataArray(eqStrain,dims=["nbtj","y","x"])
    ds['eqStress'] = xr.DataArray(eqStress,dims=["nbtj","y","x"])
    ds['act_pr'] = xr.DataArray(act_pr,dims=["nbtj","y","x"])
    ds['act_py'] = xr.DataArray(act_py,dims=["nbtj","y","x"])


    ds['dist_to_1neigh'] = xr.DataArray(dist2oTJ,dims='nbtj')
    ds['volratio_an'] = xr.DataArray(volratio_an,dims='nbtj')
    ds['sum_pix_g'] = xr.DataArray(nb_pix_g,dims='nbtj')

    return ds